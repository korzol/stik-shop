#!/usr/bin/env sh
set -e

[ ! -d '/app/vendor' ] && composer install --optimize-autoloader --no-interaction