<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        td {
            min-width: 100px;
            text-align: center;
        }
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>
                    Person, id
                </th>
                <th>
                    Want to have
                </th>
                <th>
                    Got, units
                </th>
                <th>
                    Satisfaction, %
                </th>
                <th>
                    Satisfaction unit, %
                </th>
            </tr>
        </thead>
        <tbody>
            @php($total = 0)
            @foreach($data as $key => $entry)
                <tr>
                    <td>
                        {{ $key }}
                    </td>
                    <td>
                        {{ $entry['quantity'] }}
                    </td>
                    <td>
                        {{ $entry['resolution'] }}
                    </td>
                    <td>
                        {{ round($entry['satisfaction'], 2) }}
                    </td>
                    <td>
                        {{ round($entry['satisfactionUnit'], 2) }}
                    </td>
                </tr>
                @php($total += $entry['resolution'])
            @endforeach
            <tr>
                <td></td>
                <td colspan="3" style="text-align: right">
                    Total: {{ $total }}
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
