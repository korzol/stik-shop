<?php

namespace Database\Seeders;

use App\Models\Clients;
use App\Models\OrderItems;
use App\Models\Orders;
use App\Models\Products;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = Products::factory()->create();
        Clients::factory()->count(10)->create()->each(function($client) use($product){

            $order = Orders::factory()->for($client, 'Client')->create();
            OrderItems::factory()->for($order, 'Order')->for($product, 'Product')->create();
        });
    }
}
