<?php
declare(strict_types=1);

namespace Database\Factories;

use App\Models\OrderItems;
use \Illuminate\Database\Eloquent\Factories\Factory;

class OrderItemsFactory extends Factory
{
    protected $model = OrderItems::class;

    /**
     * @inheritDoc
     * @return array{quantity: int}
     */
    public function definition(): array
    {
        return [
            'quantity' => $this->faker->numberBetween(1, 50)
        ];
    }
}
