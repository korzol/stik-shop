<?php
declare(strict_types=1);

namespace Database\Factories;

use \Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Orders;

class OrdersFactory extends Factory
{

    protected $model = Orders::class;

    /**
     * @inheritDoc
     * @return array{status: int}
     */
    public function definition(): array
    {
        return [
            'status' => 0
        ];
    }
}
