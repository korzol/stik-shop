<?php
declare(strict_types=1);

namespace Database\Factories;

use \Illuminate\Database\Eloquent\Factories\Factory;
use \App\Models\Products;

class ProductsFactory extends Factory
{

    protected $model = Products::class;

    /**
     * @inheritDoc
     * @return array{
     *     name: string,
     *     quantity: int,
     *     price: int
     * }
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->company,
            'quantity' => 20,
            'price' => $this->faker->numberBetween(10, 10000),
        ];
    }
}
