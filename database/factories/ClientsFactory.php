<?php
declare(strict_types=1);

namespace Database\Factories;

use \Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Clients;

class ClientsFactory extends Factory
{

    protected $model = Clients::class;

    /**
     * @inheritDoc
     * @return array{
     *     fname: string,
     *     lname: string
     * }
     */
    public function definition(): array
    {
        return [
            'fname' => $this->faker->firstName,
            'lname' => $this->faker->lastName,
        ];
    }
}
