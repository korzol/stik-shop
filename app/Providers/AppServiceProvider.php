<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
        if ($this->app->environment() !== 'production') {
            app('config')->set('app.aliases', [
                'Validator' => 'Illuminate\Support\Facades\Validator',
                'Eloquent'=>'Illuminate\Database\Eloquent\Model'
            ]);

            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
