<?php
declare(strict_types=1);

namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class OrderItems extends Model
{
    use HasFactory;

    protected $fillable = ['quantity'];

    public function Order(): belongsTo
    {
        return $this->belongsTo(Orders::class);
    }

    public function Product(): belongsTo
    {
        return $this->belongsTo(Products::class);
    }
}
