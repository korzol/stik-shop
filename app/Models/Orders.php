<?php
declare(strict_types=1);

namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Orders extends Model
{
    use HasFactory;

    protected $fillable = ['status'];

    public function Items(): hasMany
    {
        return $this->hasMany(OrderItems::class, 'order_id');
    }

    public function Client(): belongsTo
    {
        return $this->belongsTo(Clients::class);
    }
}
