<?php
declare(strict_types=1);

namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Clients extends Model
{
    use HasFactory;
    
    protected $fillable = ['fname', 'lname'];

    public function Orders(): HasMany
    {
        return $this->hasMany(Orders::class);
    }
}
