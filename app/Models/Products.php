<?php
declare(strict_types=1);

namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ProductsFactory
 * @mixin \Eloquent
 * @package App\Models
 */
class Products extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'quantity', 'price'];

}
