<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\StikShop\ShopClient;
use App\StikShop\ShopFactory;

class IndexController extends Controller
{
    public function main()
    {
        $shopClient = new ShopClient();

        $ordersClient = ShopFactory::getOrdersClient($shopClient);
        $productsClient = ShopFactory::getProductsClient($shopClient);

        $orders = $ordersClient->getOrders();
        $products = $productsClient->getProducts();

        $processor = ShopFactory::getOrdersProcessorClient($orders, $products);

        return view('results', ['data' => $processor->processOrders()]);
    }
}
