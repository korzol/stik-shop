<?php
declare(strict_types=1);

namespace App\StikShop;

use App\Models\Orders;
use App\Models\Products;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ShopClient implements ShopClientInterface
{
    /**
     * @inheritDoc
     */
    public function getProduct(int $id): array
    {
        try {
            return Products::findOrFail($id)->toArray();
        } catch(ModelNotFoundException $e) {
            die($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function getProducts(): array
    {
        try {
            $result = Products::orderBy('created_at', 'desc')->get()->toArray();

            if (count($result) > 0) {
                return $result;
            }

            throw new ModelNotFoundException('No products found');
        } catch (ModelNotFoundException $e) {
            die($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function saveProduct(array $product): void
    {
        $products = Products::find($product['id']);
        $products->name = $product['name'];
        $products->quantity = $product['quantity'];
        $products->save();
    }

    /**
     * @inheritDoc
     */
    public function getOrder(int $id): array
    {
        try {
            $result = Orders::where('id', $id)->with('items')->get()->toArray();
            if(count($result) > 0) {
                return $result[0];
            }

            throw new ModelNotFoundException('Order with id '.$id.' not found!');
        } catch (ModelNotFoundException $e) {
            die($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function getOrders(): array
    {
        try {
            $result = Orders::orderBy('created_at', 'desc')->with('items')->get()->toArray();

            if(count($result)  > 0) {
                return $result;
            }

            throw new ModelNotFoundException('No orders found!');

        } catch (ModelNotFoundException $e) {
            die($e->getMessage());
        }
    }
}
