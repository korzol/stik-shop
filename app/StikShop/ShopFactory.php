<?php
declare(strict_types=1);

namespace App\StikShop;


use App\StikShop\Shop\Order\OrderBuilder;
use App\StikShop\Shop\OrdersClient;
use App\StikShop\Shop\OrdersProcessorClient;
use App\StikShop\Shop\Product\ProductBuilder;
use App\StikShop\Shop\ProductsClient;
use Illuminate\Support\Collection;


class ShopFactory
{
    public static function getProductsClient(ShopClientInterface $client): ProductsClient
    {
        return new ProductsClient(
            $client,
            new ProductBuilder()
        );
    }

    public static function getOrdersClient(ShopClientInterface $client): OrdersClient
    {
        return new OrdersClient(
            $client,
            new OrderBuilder()
        );
    }

    /**
     * @param Collection $orders
     * @param Collection $products
     * @return OrdersProcessorClient
     */
    public static function getOrdersProcessorClient(Collection $orders, Collection $products): OrdersProcessorClient
    {
        return new OrdersProcessorClient(
            $orders,
            $products,
        );
    }
}
