<?php
declare(strict_types=1);

namespace App\StikShop\Shop;

use App\StikShop\Shop\Product\Product;
use App\StikShop\Shop\Product\ProductBuilderInterface;
use App\StikShop\ShopClientInterface;
use Illuminate\Support\Collection;

class ProductsClient
{
    private ShopClientInterface $client;
    private ProductBuilderInterface $productBuilder;

    public function __construct(ShopClientInterface $client, ProductBuilderInterface $productBuilder)
    {
        $this->client = $client;
        $this->productBuilder = $productBuilder;
    }

    /**
     * @param int $id
     * @return Product
     */
    public function getProduct(int $id): Product
    {
        return $this->productBuilder->buildSingleProduct(
            $this->client->getProduct($id)
        );
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {
        return $this->productBuilder->buildProductsCollection(
            $this->client->getProducts()
        );
    }

    public function updateProduct(Product $product): void
    {
        $this->client->saveProduct(
            $product->toArray()
        );
    }
}
