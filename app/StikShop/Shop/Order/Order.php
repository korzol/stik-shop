<?php
declare(strict_types=1);

namespace App\StikShop\Shop\Order;

use InvalidArgumentException;

class Order
{
    private int $id;

    private int $client_id;

    private int $status;

    private \DateTime $created_at;

    private \DateTime $updated_at;

    /**
     * @var array{
     *     array{
     *         id: int,
     *         order_id: int,
     *         product_id: int,
     *         quantity: int,
     *         resolution: int,
     *         created_at: \DateTime,
     *         updated_at: \DateTime
     *     }
     * }
     */
    private array $items;

    /**
     * Order constructor.
     * @param int $id
     * @param int $client_id
     * @param int $status
     * @param \DateTime $created_at
     * @param \DateTime $updated_at
     * @param array{
     *     array{
     *         id: int,
     *         order_id: int,
     *         product_id: int,
     *         quantity: int,
     *         resolution: int,
     *         created_at: string,
     *         updated_at: string
     *     }
     * } $items
     */
    public function __construct(int $id, int $client_id, int $status, \DateTime $created_at, \DateTime $updated_at, array $items)
    {
        $this->id = $id;
        $this->client_id = $client_id;
        $this->status = $status;
        $this->created_at = $created_at;
        $this->updated_at = $updated_at;

        try {
            $this->setItems($items);

            $this->validateId();
            $this->validateClientId();
            $this->validateStatus();
        } catch (InvalidArgumentException $e) {
            die($e->getMessage());
        }
    }

    /**
     * @param array{
     *     array{
     *         id: int,
     *         order_id: int,
     *         product_id: int,
     *         quantity: int,
     *         resolution: int,
     *         created_at: string,
     *         updated_at: string
     *     }
     * } $items
     * @throws InvalidArgumentException
     */
    private function setItems(array $items): void
    {
        try {
            foreach ($items as $key => $item) {
                $this->items[$key]['id'] = $item['id'];
                $this->items[$key]['order_id'] = $item['order_id'];
                $this->items[$key]['product_id'] = $item['product_id'];
                $this->items[$key]['quantity'] = $item['quantity'];
                $this->items[$key]['resolution'] = $item['resolution'];
                $this->items[$key]['created_at'] = new \DateTime($item['created_at']);
                $this->items[$key]['updated_at'] = new \DateTime($item['updated_at']);
            }
        } catch (\Exception $e) {
            throw new InvalidArgumentException('Abnormal order items');
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getClientId(): int
    {
        return $this->client_id;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->created_at;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updated_at;
    }

    /**
     * @return array{
     *     array{
     *         id: int,
     *         order_id: int,
     *         product_id: int,
     *         quantity: int,
     *         resolution: int,
     *         created_at: \DateTime,
     *         updated_at: \DateTime
     *     }
     * }
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function setStatus(int $status): void
    {
        // 0 - open, 1- closed. In real application may have "processing" and some other statuses as well
        $this->status = $status;
    }


    public function setResolution(int $itemId, int $resolution): void
    {
        // Under normal conditions $resolution == ordered quantity.
        // In case when there is not enough amount $resolution contains how many items they got

        foreach ($this->getItems() as $key => $item) {
            if($item['id'] === $itemId) {
                $this->items[$key]['resolution'] = $resolution;
            }
        }
    }

    private function validateId(): void
    {
        if (! $this->getId() || $this->getId() == 0 || ! is_int($this->getId())) {
            throw new InvalidArgumentException('Order must have valid id');
        }
    }

    private function validateClientId(): void
    {
        if (! $this->getClientId() || $this->getClientId() == 0 || ! is_int($this->getClientId())) {
            throw new InvalidArgumentException('Order must belong to customer');
        }
    }

    private function validateStatus(): void
    {
        if ($this->getStatus() !== 0 && $this->getStatus() !== 1) {
            throw new InvalidArgumentException('Abnormal order status');
        }
    }

    private function validateItems(): void
    {
        if (count($this->getItems()) == 0) {
            throw new InvalidArgumentException('Order must contain some items');
        }
    }

    private function validateItemsQuantity(): void
    {
        foreach ($this->getItems() as $item) {
            if (! is_int($item['quantity']) || $item['quantity'] <= 0) {
                throw new InvalidArgumentException('Item quantity should be > 0');
            }
        }
    }
}

