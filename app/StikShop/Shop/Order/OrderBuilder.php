<?php
declare(strict_types=1);

namespace App\StikShop\Shop\Order;

use Illuminate\Support\Collection;

class OrderBuilder implements OrderBuilderInterface
{
    /**
     * @param array{
     *     id: int,
     *     client_id: int,
     *     status: int,
     *     created_at: string,
     *     updated_at: string,
     *     items: array{
     *         array{
     *             id: int,
     *             order_id: int,
     *             product_id: int,
     *             quantity: int,
     *             resolution: int,
     *             created_at: string,
     *             updated_at: string
     *         }
     *     }
     * } $order
     * @return Order
     */
    public function buildSingleOrder(array $order): Order
    {
        return new Order(
            $order['id'],
            $order['client_id'],
            $order['status'],
            new \DateTime($order['created_at']),
            new \DateTime($order['updated_at']),
            $order['items']
        );
    }

    /**
     * @param array{
     *     array{
     *         id: int,
     *         client_id: int,
     *         status: int,
     *         created_at: string,
     *         updated_at: string,
     *         items: array{
     *             array{
     *                 id: int,
     *                 order_id: int,
     *                 product_id: int,
     *                 quantity: int,
     *                 resolution: int,
     *                 created_at: string,
     *                 updated_at: string
     *             }
     *         }
     *     }
     * } $orders
     * @return Collection
     */
    public function buildOrdersCollection(array $orders): Collection
    {
        $collection = collect();
        foreach($orders as $order) {
            $collection->put(
                $order['id'],
                new Order(
                    $order['id'],
                    $order['client_id'],
                    $order['status'],
                    new \DateTime($order['created_at']),
                    new \DateTime($order['updated_at']),
                    $order['items']
                )
            );
        }

        return $collection;
    }
}
