<?php
declare(strict_types=1);

namespace App\StikShop\Shop\Order;

use Illuminate\Support\Collection;

interface OrderBuilderInterface
{
    /**
     * @param array{
     *     id: int,
     *     client_id: int,
     *     status: int,
     *     created_at: \DateTime,
     *     updated_at: \DateTime,
     *     items: array{
     *         array{
     *             id: int,
     *             order_id: int,
     *             product_id: int,
     *             quantity: int,
     *             resolution: int,
     *             created_at: \DateTime,
     *             updated_at: \DateTime
     *         }
     *     }
     * } $order
     * @return Order
     */
    public function buildSingleOrder(array $order): Order;

    /**
     * @param array{
     *     array{
     *         id: int,
     *         client_id: int,
     *         status: int,
     *         created_at: \DateTime,
     *         updated_at: \DateTime,
     *         items: array{
     *             array{
     *                 id: int,
     *                 order_id: int,
     *                 product_id: int,
     *                 quantity: int,
     *                 resolution: int,
     *                 created_at: \DateTime,
     *                 updated_at: \DateTime
     *             }
     *         }
     *     }
     * } $orders
     * @return Collection
     */
    public function buildOrdersCollection(array $orders): Collection;
}
