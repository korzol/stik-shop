<?php
declare(strict_types=1);

namespace App\StikShop\Shop\Product;

use InvalidArgumentException;

class Product
{
    private int $id;

    private string $name;

    private int $quantity;

    public function __construct(int $id, string $name, int $quantity)
    {
        $this->id = $id;
        $this->name = $name;
        $this->quantity = $quantity;

        $this->validateId();
        $this->validateName();
        $this->validateQuantity();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return array{id: int, name: string, quantity: int}
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'quantity' => $this->getQuantity()
        ];
    }

    private function validateId(): void
    {
        if (! $this->id || $this->id == 0) {
            throw new InvalidArgumentException("Invalid ID");
        }
    }

    private function validateName(): void
    {
        if(! $this->name) {
            throw new InvalidArgumentException("Invalid Name");
        }
    }

    private function validateQuantity(): void
    {
        if(! $this->quantity) {
            throw new InvalidArgumentException("Invalid Quantity");
        }
    }
}
