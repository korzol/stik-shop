<?php
declare(strict_types=1);

namespace App\StikShop\Shop\Product;


use Illuminate\Support\Collection;

class ProductBuilder implements ProductBuilderInterface
{
    /**
     * @inheritDoc
     */
    public function buildSingleProduct(array $product): Product
    {
        return new Product(
            $product['id'],
            $product['name'],
            $product['quantity']
        );
    }

    /**
     * @inheritDoc
     */
    public function buildProductsCollection(array $products): Collection
    {
        $collection = collect();
        foreach ($products as $product) {
            $collection->put(
                $product['id'],
                new Product(
                    $product['id'],
                    $product['name'],
                    $product['quantity']
                )
            );
        }

        return $collection;
    }
}
