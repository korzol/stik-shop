<?php
declare(strict_types=1);

namespace App\StikShop\Shop\Product;


use Illuminate\Support\Collection;

interface ProductBuilderInterface
{
    /**
     * @param array{
     *     id: int,
     *     name: string,
     *     quantity: int
     * } $productArray
     * @return Product
     */
    public function buildSingleProduct(array $productArray): Product;

    /**
     * @param array<int, array{id: int, name: string, quantity: int}> $products
     * @return Collection
     */
    public function buildProductsCollection(array $products): Collection;
}
