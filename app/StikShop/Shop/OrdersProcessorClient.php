<?php
declare(strict_types=1);

namespace App\StikShop\Shop;

use App\StikShop\Shop\OrdersProcessor\FairDivision;
use App\StikShop\Shop\OrdersProcessor\Justice;
use App\StikShop\Shop\OrdersProcessor\OrdersDataCollector;
use App\StikShop\Shop\OrdersProcessor\ProportionalDivision;
use Illuminate\Support\Collection;

class OrdersProcessorClient
{
    /**
     * Because this is test task all orders within $order collection contain only orders for same product
     * @var Collection
     */
    private Collection $orders;

    private Collection $products;

    public function __construct(Collection $orders, Collection $products)
    {
        $this->orders = $orders;

        $this->products = $products;
    }

    /**
     * @return array<
     *     int,
     *     array{
     *         productAvailable: int,
     *         quantity: int,
     *         percent: float,
     *         resolution: int
     *     }
     * >
     */
    public function processOrders(): array
    {
        if (Justice::isPossible($this->orders, $this->products)) {
            $collector = OrdersDataCollector::collect($this->orders, $this->products);

            return FairDivision::split(
                $collector['itemsData'],
                $collector['totalOrderedQuantity']
            );
        }

        return [];
    }
}
