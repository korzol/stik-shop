<?php
declare(strict_types=1);

namespace App\StikShop\Shop;

use App\StikShop\Shop\Order\Order;
use App\StikShop\Shop\Order\OrderBuilderInterface;
use App\StikShop\ShopClientInterface;
use Illuminate\Support\Collection;


class OrdersClient
{
    private ShopClientInterface $client;
    private OrderBuilderInterface $orderBuilder;

    public function __construct(ShopClientInterface $client, OrderBuilderInterface $orderBuilder)
    {
        $this->client = $client;
        $this->orderBuilder = $orderBuilder;
    }

    public function getOrder(int $id): Order
    {
        return $this->orderBuilder->buildSingleOrder(
            $this->client->getOrder($id)
        );
    }

    public function getOrders(): Collection
    {
        return $this->orderBuilder->buildOrdersCollection(
            $this->client->getOrders()
        );
    }

    public function updateOrder(): void
    {

    }
}
