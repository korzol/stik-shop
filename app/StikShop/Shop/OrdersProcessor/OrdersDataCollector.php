<?php
declare(strict_types=1);

namespace App\StikShop\Shop\OrdersProcessor;

use Illuminate\Support\Collection;

class OrdersDataCollector
{
    /**
     * @param Collection $orders
     * @param Collection $products
     * @return array{
     *    totalOrderedQuantity: int,
     *    itemsData: array<
     *        int,
     *        array{
     *            productAvailable: int,
     *            quantity: int,
     *            percent: float,
     *            resolution: int
     *        }
     *    >
     * }
     */
    public static function collect(Collection $orders, Collection $products): array
    {
        $totalOrderedQuantity = 0;
        $itemsData = [];
        foreach ($orders as $order) {
            foreach ($order->getItems() as $item) {
                $totalOrderedQuantity += $item['quantity'];
                $itemsData[(int)$item['id']] = [
                    'productAvailable' => (int)$products->get($item['product_id'])->getQuantity(),
                    'quantity' => (int)$item['quantity'],
                    'percent' => 0,
                    'resolution' => 0,
                ];

            }
        }

        return [
            'totalOrderedQuantity' => (int)$totalOrderedQuantity,
            'itemsData' => $itemsData
        ];
    }
}
