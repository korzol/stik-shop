<?php
declare(strict_types=1);

namespace App\StikShop\Shop\OrdersProcessor;

use Exception;
use Illuminate\Support\Collection;

class Justice
{
    public static function isPossible(Collection $orders, Collection $products): bool
    {
        try {
            // To keep it simple lets pretend all orders have one item
            $productId = $orders->first()->getItems()[0]['product_id'];
            $productQuantity = $products->get($productId)->getQuantity();

            // There is not enough product quantity to satisfy orders
            // even if we give out one product unit per order
            if (count($orders) > $productQuantity) {
                throw new Exception('There is not enough product amount to distribute at least one per order');
            }

            return true;
        } catch(Exception $e) {
            die($e->getMessage());
        }
    }
}
