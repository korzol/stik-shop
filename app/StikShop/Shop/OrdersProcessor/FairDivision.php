<?php
declare(strict_types=1);

namespace App\StikShop\Shop\OrdersProcessor;


class FairDivision
{
    /**
     * @param array<
     *     int,
     *     array{
     *         productAvailable: int,
     *         quantity: int,
     *         percent: float,
     *         resolution: int
     *     }
     * > $itemsData
     * @param int $totalOrderedQuantity
     * @return array<
     *     int,
     *     array{
     *         productAvailable: int,
     *         quantity: int,
     *         percent: float,
     *         resolution: int
     *     }
     * >
     */
    public static function split(array $itemsData, int $totalOrderedQuantity): array
    {
        $itemsData = self::initialDivide($itemsData);

        $maxPercent = self::mostValuableUnit($itemsData);

        $totalProduct = $itemsData[1]['productAvailable'] - count($itemsData);

        $totalProduct = $totalProduct > $totalOrderedQuantity ? $totalOrderedQuantity- count($itemsData) : $totalProduct;

        $leader = $maxPercent;

        while($totalProduct > 0) {
            $maxSatisfaction = 0;
            $id = self::minSatisfied($itemsData);

            if (
                ($itemsData[$id]['satisfaction'] + $itemsData[$id]['satisfactionUnit']) <= $itemsData[$leader['id']]['satisfaction'] + 10 &&
                $totalProduct > 0
            ) {
                $itemsData[$id]['satisfaction'] += $itemsData[$id]['satisfactionUnit'];
                $itemsData[$id]['resolution'] += 1;
                $totalProduct -= 1;

                if($itemsData[$id]['satisfaction'] > $maxSatisfaction) {
                    $maxSatisfaction = $itemsData[$id]['satisfaction'];
                }
            }

            if (
                ($maxSatisfaction > $itemsData[$leader['id']]['satisfaction']) &&
                $totalProduct > 0 &&
                ($itemsData[$leader['id']]['satisfaction'] + $itemsData[$leader['id']]['satisfactionUnit']) <= 100
            ) {
                $itemsData[$leader['id']]['satisfaction'] += $itemsData[$leader['id']]['satisfactionUnit'];
                $itemsData[$leader['id']]['resolution'] += 1;
                $totalProduct -= 1;
                continue;
            }
        }

        return $itemsData;
    }

    /**
     * @param array<
     *     int,
     *     array{
     *         productAvailable: int,
     *         quantity: int,
     *         percent: float,
     *         resolution: int,
     *         satisfaction: float,
     *         satisfactionUnit: float
     *     }
     * > $itemData
     * @return int
     */
    public static function minSatisfied(array $itemData): int
    {
        $min = 1000;
        $minId = 0;
        foreach ($itemData as $id => $item) {
            if($itemData[$id]['satisfaction'] < $min) {
                $minId = (int)$id;
                $min = $itemData[$id]['satisfaction'];
            }
        }

        return $minId;
    }

    /**
     * @param array<
     *     int,
     *     array{
     *         productAvailable: int,
     *         quantity: int,
     *         percent: float,
     *         resolution: int
     *     }
     * > $itemsData
     * @return array{id: int, percent: float}
     */
    public static function mostValuableUnit(array &$itemsData): array
    {
        $max = 0;
        $maxId = 0;
        foreach ($itemsData as $id => $item) {
            $result = 100 / $item['quantity'];
            if ($result > $max) {
                $maxId = $id;
                $max = $result;
            }

            $itemsData[$id]['satisfactionUnit'] = $result;
            $itemsData[$id]['satisfaction'] = $result;
        }

        return [
            'id' => $maxId,
            'percent' => $max
        ];
    }

    /**
     * @param array<
     *     int,
     *     array{
     *         productAvailable: int,
     *         quantity: int,
     *         percent: float,
     *         resolution: int
     *     }
     * > $itemsData
     * @return array<
     *     int,
     *     array{
     *         productAvailable: int,
     *         quantity: int,
     *         percent: float,
     *         resolution: int
     *     }
     * >
     */
    public static function initialDivide(array $itemsData): array
    {
        foreach ($itemsData as $id => $item) {
            $itemsData[$id]['resolution'] = 1;
        }

        return $itemsData;
    }
}
