<?php
declare(strict_types=1);

namespace App\StikShop;

interface ShopClientInterface
{
    /**
     * @param int $id
     * @return array{id: int, name: string, quantity: int}
     */
    public function getProduct(int $id): array;

    /**
     * @return array<int, array{id: int, name: string, quantity: int}>
     */
    public function getProducts(): array;

    /**
     * @param array{id: int, name: string, quantity: int} $product
     */
    public function saveProduct(array $product): void;

    /**
     * @param int $id
     * @return array{
     *     id: int,
     *     client_id: int,
     *     status: int,
     *     created_at: \DateTime,
     *     updated_at: \DateTime,
     *     items: array{
     *         array{
     *             id: int,
     *             order_id: int,
     *             product_id: int,
     *             quantity: int,
     *             resolution: int,
     *             created_at: \DateTime,
     *             updated_at: \DateTime
     *         }
     *     }
     * }
     */
    public function getOrder(int $id): array;

    /**
     * @return array{
     *     array{
     *         id: int,
     *         client_id: int,
     *         status: int,
     *         created_at: \DateTime,
     *         updated_at: \DateTime,
     *         items: array{
     *             array{
     *                 id: int,
     *                 order_id: int,
     *                 product_id: int,
     *                 quantity: int,
     *                 resolution: int,
     *                 created_at: \DateTime,
     *                 updated_at: \DateTime
     *             }
     *         }
     *     }
     * }
     */
    public function getOrders(): array;
}
