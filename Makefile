.PHONY: help
.DEFAULT_GOAL := help
SHELL=bash

dc_path=docker/docker-compose.yml
app_container=lumen_app
db_container=db

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

init: up compose ## Initialise environment on a first start

up: ## Start local environment
	docker-compose -f ${dc_path} up -d --build

down: ## Stop local environemtn
	docker-compose -f ${dc_path} down

restart: down up ## Restart environment

destroy: ## Destroy local environment
	docker-compose -f ${dc_path} down --volumes --remove-orphans
	docker-compose -f ${dc_path} rm -vsf

compose: ## Run composer install
	docker-compose -f ${dc_path} exec -T ${app_container} composer install --optimize-autoloader --no-interaction

reseed: reset_db seed_db ## Reset db and seed with new dataset

setup_db: ## Setup DB
	docker-compose -f ${dc_path} exec -T ${app_container} php ./artisan migrate

reset_db: ## Reset DB (atisan migrate:fresh)
	docker-compose -f ${dc_path} exec -T ${app_container} php ./artisan migrate:fresh

seed_db: ## Seed DB (atisan db:seed)
	docker-compose -f ${dc_path} exec -T ${app_container} php ./artisan db:seed

