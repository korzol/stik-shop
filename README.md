In order to setup this app you need to have docker and docker-compose installed. 

Clone the code, like this:
```
git clone https://gitlab.com/korzol/stik-shop.git && cd stik-shop
```


Next you need to run the following command:
```
make init
```

It will build the docker containers.

Next you need to setup and seed db:
```
make setup_db
make seed_db
```

If the commands above output something like 'SQLSTATE[HY000] [2002] Connection refused' then you have to wait a little until mysql ready to accept connection. Ususally it should not take more than 30 seconds.

Once database is ready and filled in with data, you can access html page on the following [link](127.0.0.1:8086)
Additionaly you may want to access phpmyadmin, you may do so by access the [link](127.0.0.1:8087)


Feel free to refresh db by issuing:
```
make reset_db
make seed_db
```


Feel free to check out the [demo](http://184.173.50.54:8086)
